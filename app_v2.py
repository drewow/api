from flask import Flask, request, jsonify
import time
from flask_cors import CORS, cross_origin
import boto3
import io
from PIL import Image, ImageDraw, ImageFont
from botocore.exceptions import ClientError
import pytesseract
from pytesseract import Output
import cv2
import numpy as np
from imutils.object_detection import non_max_suppression
import argparse
import mysql.connector
from pprint import pprint
from jose import jwt, jwk
from jose.utils import base64url_decode
import requests

app = Flask(__name__)
#CORS(app, support_credentials=True)
CORS(app, resources={r"/*": {"origins": "*"}})

bucket = 'blockandwine'

ap = argparse.ArgumentParser()
ap.add_argument("-east", "--east", type=str,
	help="path to input EAST text detector", default="/data/BLOCKWINE/frozen_east_text_detection.pb")
ap.add_argument("-c", "--min-confidence", type=float, default=0.5,
	help="minimum probability required to inspect a region")
ap.add_argument("-w", "--width", type=int, default=320,
	help="resized image width (should be multiple of 32)")
ap.add_argument("-e", "--height", type=int, default=320,
	help="resized image height (should be multiple of 32)")
args = vars(ap.parse_args())


########################
#       Functions
########################

def getPictureFromS3(key):

    bucket = 'blockandwine'

    # Get the service client with sigv4 configured
    s3 = boto3.client('s3')
    url = s3.generate_presigned_url(
        ClientMethod='get_object',
        Params={
            'Bucket': bucket,
            'Key': key
        },
        ExpiresIn=300
    )
    return url

def downloadToLocal(document):
    
    s3 = boto3.resource('s3')

    url = getPictureFromS3(document)

    try:
        path = '/data/BLOCKWINE/temp/1_file.jpg'
        s3.Bucket(bucket).download_file(
            document, path)
        return path
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise

def process_image(url):
    
    path = downloadToLocal(url)
    # load the input image and grab the image dimensions
    image = cv2.imread(path)
    orig = image.copy()
    (H, W) = image.shape[:2]

    # set the new width and height and then determine the ratio in change
    # for both the width and height
    (newW, newH) = (args["width"], args["height"])
    rW = W / float(newW)
    rH = H / float(newH)

    # resize the image and grab the new image dimensions
    image = cv2.resize(image, (newW, newH))
    (H, W) = image.shape[:2]

    # define the two output layer names for the EAST detector model that
    # we are interested -- the first is the output probabilities and the
    # second can be used to derive the bounding box coordinates of text
    layerNames = [
	"feature_fusion/Conv_7/Sigmoid",
	"feature_fusion/concat_3"]

    # load the pre-trained EAST text detector
    print("[INFO] loading EAST text detector...")
    net = cv2.dnn.readNet(args["east"])

    # construct a blob from the image and then perform a forward pass of
    # the model to obtain the two output layer sets
    blob = cv2.dnn.blobFromImage(image, 1.0, (W, H),
	(123.68, 116.78, 103.94), swapRB=True, crop=False)
    start = time.time()
    net.setInput(blob)
    (scores, geometry) = net.forward(layerNames)
    end = time.time()

    # show timing information on text prediction
    print("[INFO] text detection took {:.6f} seconds".format(end - start))

    # grab the number of rows and columns from the scores volume, then
    # initialize our set of bounding box rectangles and corresponding
    # confidence scores
    (numRows, numCols) = scores.shape[2:4]
    rects = []
    confidences = []

    # loop over the number of rows
    for y in range(0, numRows):
        # extract the scores (probabilities), followed by the geometrical
	# data used to derive potential bounding box coordinates that
	# surround text
        scoresData = scores[0, 0, y]
        xData0 = geometry[0, 0, y]
        xData1 = geometry[0, 1, y]
        xData2 = geometry[0, 2, y]
        xData3 = geometry[0, 3, y]
        anglesData = geometry[0, 4, y]

        # loop over the number of columns
        for x in range(0, numCols):
            # if our score does not have sufficient probability, ignore it
            if scoresData[x] < args["min_confidence"]:
                continue

            # compute the offset factor as our resulting feature maps will
            # be 4x smaller than the input image
            (offsetX, offsetY) = (x * 4.0, y * 4.0)

            # extract the rotation angle for the prediction and then
            # compute the sin and cosine
            angle = anglesData[x]
            cos = np.cos(angle)
            sin = np.sin(angle)

            # use the geometry volume to derive the width and height of
            # the bounding box
            h = xData0[x] + xData2[x]
            w = xData1[x] + xData3[x]

            # compute both the starting and ending (x, y)-coordinates for
            # the text prediction bounding box
            endX = int(offsetX + (cos * xData1[x]) + (sin * xData2[x]))
            endY = int(offsetY - (sin * xData1[x]) + (cos * xData2[x]))
            startX = int(endX - w)
            startY = int(endY - h)

            # add the bounding box coordinates and probability score to
            # our respective lists
            rects.append((startX, startY, endX, endY))
            confidences.append(scoresData[x])

    # apply non-maxima suppression to suppress weak, overlapping bounding
    # boxes
    boxes = non_max_suppression(np.array(rects), probs=confidences)

    # loop over the bounding boxes
    text = ''
    for (startX, startY, endX, endY) in boxes:
        # scale the bounding box coordinates based on the respective
        # ratios
        startX = int(startX * rW)
        startY = int(startY * rH)
        endX = int(endX * rW)
        endY = int(endY * rH)

        # draw the bounding box on the image
        cv2.rectangle(orig, (startX, startY), (endX, endY), (0, 255, 0), 2)

        # extract the region of interest
        r = orig[startY:endY, startX:endX]

        # configuration setting to convert image to string.
        configuration = ("-l spa --oem 1 --psm 8")
        # This will recognize the text from the image of bounding box
        text += pytesseract.image_to_string(r, config=configuration)
    return text

def introduceRoute(idUser, lpn, time, gps, lpn_image = '', weight = 0, weight_img = '', netto = 0, netto_img = '', degrees = 0, degrees_img = ''):
    # MySQL statements
    cnx = mysql.connector.connect(user="root", password="PePe8810!@@", database="BW_DB")
    cursor = cnx.cursor()

    try:
        add_route = """INSERT INTO Routes 
        (id_user, date, lpn, lpn_img, weight, weight_img, netto, netto_img, degrees, degrees_img)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"""

        data_route = (idUser, time, lpn, lpn_image, weight, weight_img, netto, netto_img, degrees, degrees_img)
        cursor.execute(add_route, data_route)

        # Make sure data is committed to the database
        cnx.commit()

        id_route = cursor.lastrowid
        cursor.close()
        cnx.close()
        trace_id = updateCoordinates(id_route, time, gps, 1)

        return id_route
    except IndentationError:
        print('A MySQL error took place')

def updateCoordinates(id_route, time, gps, code):
    # MySQL statements
    cnx = mysql.connector.connect(user="root", password="PePe8810!@@", database="BW_DB")
    cursor = cnx.cursor()

    try:
        add_route = """INSERT INTO Traces 
        (id_route, time, gps, code)
        VALUES (%s, %s, %s, %s);"""

        data_route = (id_route, time, gps, code)
        cursor.execute(add_route, data_route)

        # Make sure data is committed to the database
        cnx.commit()

        id_trace = cursor.lastrowid
        cursor.close()
        cnx.close()
        return id_trace
    except IndentationError:
        print('A MySQL error took place')

def finishRouteDef(id_route, time, gps, weight = 0, weight_img = '', netto = 0, netto_img = '', degrees = 0, degrees_img = ''):
    # MySQL statements
    cnx = mysql.connector.connect(user="root", password="PePe8810!@@", database="BW_DB")
    cursor = cnx.cursor()

    try:
        add_route = """UPDATE Routes 
        SET weight = %s, weight_img = %s, netto = %s, netto_img = %s, degrees = %s, degrees_img = %s
        WHERE id = %s;"""

        data_route = (weight, weight_img, netto, netto_img, degrees, degrees_img, id_route)
        cursor.execute(add_route, data_route)

        # Make sure data is committed to the database
        cnx.commit()

        cursor.close()
        cnx.close()
        trace_id = updateCoordinates(id_route, time, gps, 3)

        return trace_id
    except IndentationError:
        print('A MySQL error took place')

def get_user_id_from_mail(mail):

    cnx = mysql.connector.connect(user="root", password="PePe8810!@@", database="BW_DB")
    cursor = cnx.cursor()

    try:
        get_idUser = "SELECT * FROM User WHERE name = %s"
        cursor.execute(get_idUser, (mail,))
        idUser = cursor.fetchone()
        print(str(idUser))
        return(int(idUser[0]))
    except IndentationError:
        return(-1)

def getRoutes(id_user):

    cnx = mysql.connector.connect(user="root", password="PePe8810!@@", database="BW_DB")
    cursor = cnx.cursor()

    try:
        get_routes = "SELECT * FROM Routes WHERE id_user = %s"

        arrayReturn = {}
        arrayReturn['Routes'] = []

        cursor.execute(get_routes, (id_user,))
        records = cursor.fetchall()
        cursor.reset()
        for row in records:
            idR = row[0]
            idUser = row[1]
            date = row[2]
            lpn = row[3]
            lpn_img = row[4]
            weight = row[5]
            weight_img = row[6]
            netto = row[7]
            netto_img = row[8]
            degrees = row[9]
            degrees_img = row[10]

            get_traces = "SELECT * FROM Traces WHERE id_route = %s"
            cursor.execute(get_traces, (idR,))
            records_t = cursor.fetchall()
            cursor.reset()
            outsideObj = {"id": idR,
                    "traces": []}

            for row_t in records_t:
                entry_id = row_t[0]
                route_id = row_t[1]
                time = row_t[2]
                gps = row_t[3]
                code = row_t[4]
                
                if int(code) < 0:
                    insideObj = {"entry_id" : entry_id,
                        "code": int(code),
                        "lpn": lpn,
                        "gps": gps,
                        "time": time,
                        "lpn_photo": lpn_img}
                else:
                    insideObj = {"entry_id" : int(entry_id),
                        "code": int(code),
                        "lpn": lpn,
                        "gps": gps,
                        "time": time,
                        "lpn_photo": lpn_img,
                        "weight": float(weight),
                        "weight_img": weight_img,
                        "netto": float(netto),
                        "netto_img": netto_img,
                        "degrees": float(degrees),
                        "degrees_img": degrees_img}
                
                outsideObj["traces"].append(insideObj)

            arrayReturn['Routes'].append(outsideObj)
        
        cursor.close()
        cnx.close()
        return arrayReturn

    except IndentationError:
        return None

def process_login(username, password, user_pool_id, app_client_id):

    client = boto3.client('cognito-idp')

    resp = client.admin_initiate_auth(
        UserPoolId=user_pool_id,
        ClientId=app_client_id,
        AuthFlow='ADMIN_NO_SRP_AUTH',
        AuthParameters={
            "USERNAME": username,
            "PASSWORD": password
        }
    )

    print("Log in success")
    #print("Access token:", resp['AuthenticationResult']['AccessToken'])
    #print("ID token:", resp['AuthenticationResult']['IdToken'])
    print(resp)

    access_token = \
        resp['AuthenticationResult']['AccessToken']
    id_token = \
        resp['AuthenticationResult']['IdToken']
    refresh_token = \
        resp['AuthenticationResult']['RefreshToken']

    return id_token, access_token, refresh_token 

def check_token(id_token, access_token):
    # AWS docs on verifying tokens:
    # https://amzn.to/2vUwFx7
    # Decode token header
    token_header = jwt.get_unverified_header(id_token)
    print('Token header:')
    pprint(token_header)
    # Decode token payload
    token_claims = jwt.get_unverified_claims(id_token)
    print('Token claims:')
    pprint(token_claims)
    # Verify signature, step by step.
    # Original (and better) code in this gist: https://bit.ly/2E3fAFP
    print('Checking key manually')
    # First, get the JSON Web Key Set, which contains two public
    # keys corresponding to the two private keys that could
    # have been used to sign the token.
    r = requests.get(deadpool['jwks_url'])
    if r.status_code == 200:
        jwks = r.json()
    else:
        return(-1)
        #raise 'Did not retrieve JWKS, got {}'.format(r.status_code)
    # The token header contains a field named 'kid', which stands
    # for Key ID. The JWKS also contains two 'kid' fields, one for
    # each key. The 'kid' in the header tells us which public key
    # must be used to verify the signature.
    kid = token_header['kid']
    # Search the JWKS for the proper public key
    key_index = -1
    for i in range(len(jwks['keys'])):
        if kid == jwks['keys'][i]['kid']:
            key_index = i
            break
    if key_index == -1:
        print('Public key not found, can not verify token')
        return(-1)
    else:
        # Convert public key
        public_key = jwk.construct(jwks['keys'][key_index])
        # Get claims and signature from token
        claims, encoded_signature = id_token.rsplit('.', 1)
        # Verify signature
        decoded_signature = base64url_decode(
                encoded_signature.encode('utf-8'))
        if not public_key.verify(claims.encode("utf8"),
                                 decoded_signature):
            print('Signature verification failed')
            return(-1)
        else:
            print('Signature successfully verified')
            decode_token(access_token)

def decode_token(access_token):
    # Executing decode() on the token will return the header or raise
    # an error if checking the signature or one of the claims fails.
    # See https://python-jose.readthedocs.io/en/latest/jwt/api.html
    
    try:
        auth = access_token.split(maxsplit=2)  # only interested in the first two parts
        token = b64token = None
        if len(auth) > 1 and auth[0].lower() == 'bearer':
            b64token = auth[1]
        else:
            pprint('Bad token formation')
            return(-1)
        result = jwt.decode(
            b64token,
            requests.get(deadpool['jwks_url']).json())
        pprint(result['username'])
        idUser = get_user_id_from_mail(result['username'])
        return idUser
    except IndentationError:
        pprint('Bad token')
        return(-1)

@app.route('/api/v001/login', methods=['POST'])
def login():
    if not request.json or not 'username' in request.json or not 'password' in request.json:
        return jsonify({'error' : "Data not correctly entered"})
    else:
        username = request.json['username']
        password = request.json['password']
        token = process_login(username, password, 'eu-west-1_btUw5jhWE', '5j3aob9pivp23io6741ttc5hg9')

        if token != -1:
            return jsonify({'id': 1, 'value': token})
        else:
            return jsonify({'id': -1, 'value': -1})


@app.route('/api/v001/checkLicensePlate', methods=['POST'])
def checkLicensePlate():
    userId = decode_token(request.headers['Authorization'])
    if userId == -1:
        return jsonify({'error' : "Bad token"})
    if not request.json or not 'url' in request.json:
        return jsonify({'error' : "S3 url missing"})
    else:
        url = request.json['url']
        lpn = "E6308BHD"#process_image(url)

        return jsonify({'id': 1, 'value': lpn})

@app.route('/api/v001/startRoute', methods=['POST'])
def startRoute():
    userId = decode_token(request.headers['Authorization'])
    if userId == -1:
        return jsonify({'error' : "Bad token"})
    if not request.json or not 'lpn' in request.json or not 'time' in request.json or not 'gps' in request.json or not 'lpn_image' in request.json:
        return jsonify({'error' : "data missing"})
    else:
        lpn = request.json['lpn']
        time = request.json['time']
        gps = request.json['gps']
        lpn_image = request.json['lpn_image']
        id_route = introduceRoute(userId, lpn, time, gps, lpn_image = lpn_image)

        return jsonify({'status': 'transit', 'route': id_route, 'code': 1})

@app.route('/api/v001/updateCoords', methods=['POST'])
def updateCoords():
    userId = decode_token(request.headers['Authorization'])
    if userId == -1:
        return jsonify({'error' : "Bad token"})
    if not request.json or not 'route_id' in request.json or not 'time' in request.json or not 'gps' in request.json:
        return jsonify({'error' : "data missing"})
    else:
        route_id = request.json['route_id']
        time = request.json['time']
        gps = request.json['gps']
        trace_id = updateCoordinates(route_id, time, gps, 2)

        return jsonify({'status': 'transit', 'route': route_id, 'entry_id': trace_id, 'code': 2})

@app.route('/api/v001/checkValueConsole', methods=['POST'])
def checkValueConsole():
    userId = decode_token(request.headers['Authorization'])
    if userId == -1:
        return jsonify({'error' : "Bad token"})
    if not request.json or not 'id' in request.json or not 'url' in request.json:
        return jsonify({'error' : "data missing"})
    else:
        url = request.json['url']
        code = request.json['id']
        if code == 2:
            value = 9840
        elif code == 3:
            value = 6840
        else:
            value = 13.0
        return jsonify({'id': code, 'value': value})

@app.route('/api/v001/finishRoute', methods=['POST'])
def finishRoute():
    userId = decode_token(request.headers['Authorization'])
    if userId == -1:
        return jsonify({'error' : "Bad token"})
    if not request.json or not 'route_id' in request.json or not 'time' in request.json or not 'gps' in request.json:
        return jsonify({'error' : "data missing"})
    else:
        route_id = request.json['route_id']
        time = request.json['time']
        gps = request.json['gps']
        weight = request.json['weight']
        weight_img = request.json['weight_img']
        netto = request.json['netto']
        netto_img = request.json['netto_img']
        degrees = request.json['degrees']
        degrees_img = request.json['degrees_img']

        trace_id = finishRouteDef(route_id, time, gps, weight, weight_img, netto, netto_img, degrees, degrees_img)

        return jsonify({'status': 'finished', 'route': route_id, 'entry_id': trace_id, 'code': 3, 'doc': ''})

@app.route('/api/v001/getAllRoutes', methods=['POST'])
def getAllRoutes():
    userId = decode_token(request.headers['Authorization'])
    if userId == -1:
        return jsonify({'error' : "Bad token"})
    else:
        jsonObj = getRoutes(userId)

        return jsonify(jsonObj)

if __name__ == '__main__':
    #app.run(host='0.0.0.0', port=5003, threaded=False)
   
    ######
    global deadpool     # Yes, global. This isn't production code
    deadpool = {}
    # Put in your own values. These are fake
    deadpool['user_pool_id'] = 'eu-west-1_btUw5jhWE'
    deadpool['region'] = 'eu-west-1'
    deadpool['jwks_url'] = 'https://cognito-idp.{}.amazonaws.com/{}/' \
                           '.well-known/jwks.json'.format(
                                   deadpool['region'],
                                   deadpool['user_pool_id'])
    deadpool['app_client_id'] = '3rb9mhrfqme2lbjepb353jrlml'

    ######
    app.run(host='0.0.0.0', port=5003, threaded=False)

    #id_token, access_token, refresh_token = process_login('juanba.tomas@gmail.com', 'PePe8810?)', 'eu-west-1_btUw5jhWE', '5j3aob9pivp23io6741ttc5hg9')
    
    #check_token(id_token, access_token)
    #decode_token('Bearer '+access_token)
