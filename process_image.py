#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pytesseract
from pytesseract import Output
import cv2
import numpy as np


# In[2]:


img = cv2.imread('temp/1_file.jpg', cv2.IMREAD_UNCHANGED)
scale_percent = 200 # percent of original size
width = int(img.shape[1] * scale_percent / 100)
height = int(img.shape[0] * scale_percent / 100)
dim = (width, height)
# resize image
resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)


# In[4]:


d = pytesseract.image_to_data(resized, lang='spa', output_type=Output.DICT)
n_boxes = len(d['level'])
for i in range(n_boxes):
    (x, y, w, h) = (d['left'][i], d['top'][i], d['width'][i], d['height'][i])
    ar = w/h
    
    if ar >15.0 or ar<1.0:
        continue
    cv2.rectangle(resized, (x, y), (x + w, y + h), (0, 255, 0), 2)
    print(d['text'][i])

cv2.imwrite('result.jpg', resized)


# In[ ]:




